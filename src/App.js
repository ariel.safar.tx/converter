import React, { Component } from 'react';
import { fetchCurrencies } from './currencies.service';
import classNames from 'classnames';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { Currencies: {}, activeCurrency: "ILS", inputValue: undefined }
  }

  componentWillMount() {
    fetchCurrencies().then(Currencies => {
      console.log(Currencies);
      this.setState({ Currencies })
    });
  }

  setActiveCurrency = (activeCurrency) => {
    this.setState({ activeCurrency });
  }

  convertCurrency = (to, from, value = (this.state.inputValue || 0)) => {
    if (from === to) {
      return value;
    }

    return this.state.Currencies[from] ? Math.round(this.state.Currencies[from][to] * value * 100) / 100 : 0;
  }
  render() {
    const { activeCurrency, inputValue } = this.state;
    return (
      <div className="App">
        <div className={'currency-selector'}>
          <button className={classNames('currency', { active: activeCurrency === "ILS" })} onClick={() => { this.setActiveCurrency("ILS") }} >שקל</button>
          <button className={classNames('currency', { active: activeCurrency === "EUR" })} onClick={() => { this.setActiveCurrency("EUR") }} >אירו</button>
          <button className={classNames('currency', { active: activeCurrency === "HRK" })} onClick={() => { this.setActiveCurrency("HRK") }} >קונה</button>
          <button className={classNames('currency', { active: activeCurrency === "USD" })} onClick={() => { this.setActiveCurrency("USD") }} >דולר</button>
        </div>

        <input className={'currency-input'} type='number' placeholder={"הכנס ערך"} value={inputValue} onChange={e => this.setState({ inputValue: (e.target.value !== "" ? Number(e.target.value) : undefined) })} />
        <div className='result-section'>
          <div className={'currency-result'}>
            {`${this.convertCurrency(activeCurrency, "ILS").toLocaleString()} ₪`}
          </div>
          <div className={'currency-result'}>
            {`${this.convertCurrency(activeCurrency, "EUR").toLocaleString()} €`}
          </div>
          <div className={'currency-result'}>
            {`${this.convertCurrency(activeCurrency, "HRK").toLocaleString()} kn`}
          </div>
          <div className={'currency-result'}>
            {`${this.convertCurrency(activeCurrency, "USD").toLocaleString()} $`}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
